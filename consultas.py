import sqlite3, sys
from sqlite3 import Error

from PyQt5.QtWidgets import QDialog, QApplication, QTableWidgetItem

from plantilla_BDProductos import * 


class Formulario(QDialog):


    def __init__(self):

        super().__init__()

        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        self.ui.botonAltasNuevaFila.clicked.connect(self.CrearRegistro)

        self.ui.botonModificacionNuevoPrecio.clicked.connect(self.ActualizarRegistro)

        self.ui.botonImpresionVerTabla.clicked.connect(self.LeerRegistro)

        self.ui.botonBajasDesechar.clicked.connect(self.BorrarRegistro)

        self.ui.botonBajasSi.clicked.connect(self.ConfirmarBorrado)
        self.ui.etiquetaBajasSeguro.hide()
        self.ui.botonBajasSi.hide()
        self.ui.botonBajasNo.hide()

        self.ui.botonConsultasExplorar.clicked.connect(self.BuscarDatos)

        self.show()


    def CrearRegistro(self):

        crear_sqlInsert = "INSERT INTO " + self.ui.entradaAltasTablaNombre.text() + " VALUES('" + self.ui.entradaAltasCodigo.text() + "', '" + self.ui.entradaAltasProducto.text() + "', '" + self.ui.entradaAltasDescripcion.text() + "', '" + self.ui.entradaAltasPrecio.text() + "')"

        try:

            conn = sqlite3.connect(self.ui.entradaAltasBaseDatosNombre.text() + ".db")

            with conn:

                curs = conn.cursor()
                curs.execute(crear_sqlInsert)

                conn.commit()

            self.ui.etiquetaAltasMensaje.setText("Fila insertada con exito")

        except Error as e:

            self.ui.etiquetaAltasMensaje.setText("Error al insertar: Fila")

        finally:

            conn.close()


    def ActualizarRegistro(self):

         actualizar_sqlSelect = "SELECT codigo, precio FROM productos WHERE codigo like '" + self.ui.entradaModificacionCodigo.text() + "' and precio like '" + self.ui.entradaModificacionPrecioAntes.text() + "'"

         try:

             conn = sqlite3.connect("productosDB.db")

             curs = conn.cursor()
             curs.execute(actualizar_sqlSelect)

             fila_en_actualizar = curs.fetchone()

             if fila_en_actualizar == None:

                 self.ui.etiquetaModificacionMensaje.setText("Codigo o precio incorrecto")

             else:

                 if self.ui.entradaModificacionPrecioAhora.text() == self.ui.entradaModificacionReingresePrecioAhora.text():

                     actualizar_sqlUpdate = "UPDATE productos set precio = '" + self.ui.entradaModificacionPrecioAhora.text() + "' WHERE codigo like '" + self.ui.entradaModificacionCodigo.text() + "'" 

                     with conn:

                         curs.execute(actualizar_sqlUpdate)

                         self.ui.etiquetaModificacionMensaje.setText("Precio actualizado con exito")
                 else:

                     self.ui.etiquetaModificacionMensaje.setText("No hay coincidencia entre precios")

         except Error as e:

             self.ui.etiquetaModificacionMensaje.setText("Error de acceso: Fila")

         finally:

             conn.close()


    def LeerRegistro(self):

        leer_sqlSelect = "SELECT * FROM " + self.ui.entradaImpresionTablaNombre.text()

        try:

            conn = sqlite3.connect(self.ui.entradaImpresionBaseDatosNombre.text()+ ".db")

            curs = conn.cursor()

            curs.execute(leer_sqlSelect)

            filas_en_leer = curs.fetchall()


            # completar...


        except Error as e:

            self.ui.tableWidget.clearContents()

            self.ui.etiquetaImpresionMensaje.setText("Error de acceso: Tabla")

        finally:

            conn.close()


    def BorrarRegistro(self):

        borrar_sqlSelect = "SELECT codigo FROM productos WHERE codigo like '" + self.ui.entradaBajasEliminarCodigo.text() + "'"

        try:

            conn = sqlite3.connect("productosDB.db")

            curs = conn.cursor()
            curs.execute(borrar_sqlSelect)

            fila_en_borrar = curs.fetchone()

            if fila_en_borrar == None:

                self.ui.etiquetaBajasSeguro.hide()
                self.ui.botonBajasSi.hide()
                self.ui.botonBajasNo.hide()

                self.ui.etiquetaBajasMensaje.setText("Codigo incorrecto")

            else:

                self.ui.etiquetaBajasSeguro.show()
                self.ui.botonBajasSi.show()
                self.ui.botonBajasNo.show()

                self.ui.etiquetaBajasMensaje.setText("")

        except Error as e:

            self.ui.etiquetaBajasMensaje.setText("Error de acceso: Producto")

        finally:

            conn.close()


    def ConfirmarBorrado(self):

        borrar_sqlDelete = "DELETE FROM productos WHERE codigo like '" + self.ui.entradaBajasEliminarCodigo.text() + "'"

        try:

            conn = sqlite3.connect("productosDB.db")

            curs = conn.cursor()

            with conn:

                curs.execute(borrar_sqlDelete)

                self.ui.etiquetaBajasMensaje.setText("Producto eliminado con exito")
                
        except Error as e:

            self.ui.etiquetaBajasMensaje.setText("Error al eliminar: Producto")

        finally:

            conn.close()


    def BuscarDatos(self):

        buscar_sqlSelect = "SELECT producto FROM " + self.ui.entradaConsultasTablaNombre.text() + " WHERE codigo like '" + self.ui.entradaConsultasBuscarCodigo.text() + "'"

        try:

            conn = sqlite3.connect(self.ui.entradaConsultasBaseDatosNombre.text() + ".db")

            curs = conn.cursor()
            curs.execute(buscar_sqlSelect)

            fila_en_buscar = curs.fetchone()

            if fila_en_buscar == None:

                self.ui.etiquetaConsultasMensaje.setText("Producto no encontrado")

                self.ui.salidaConsultasProductoHallado.setText("")

            else:

                self.ui.etiquetaConsultasMensaje.setText("Producto encontrado")

                self.ui.salidaConsultasProductoHallado.setText(fila_en_buscar[0])

        except Error as e:

            self.ui.etiquetaConsultasMensaje.setText("Error de acceso: Fila")

        finally:

            conn.close()
 

if __name__ == "__main__":

    app = QApplication(sys.argv)

    form = Formulario()
    form.show()

    sys.exit(app.exec_())